
//scripts will be here
$(document).ready(()=>{
    //initialize functions

    //toastr notif
    const notification= (type,title,message) => {
        return toastr[type](message,title );
    };
    //form reset
    formReset = (action = "hide") => {
        $("html, body").animate({ scrollTop: 0 }, "slow");
    
        if (action == "hide") {
            // hide and clear form
            $("form")[0].reset();
            $("#div_form").hide();
           // $(".form_container").hide();
            $("#btn-add").show();
        } else if (action == "show") {
            // show
            $("#div_form").show();
           // $(".form_container").show();
            $("#btn-add").hide();
            $(".submit").show();
            $("form input, select, textarea").prop("disabled", false);
            $("form button").prop("disabled", false);
            
        }
    };
    //call functions
    //datatable
    $('#student_info').DataTable();
    formReset("hide");

    //form validate

    $('form').on('submit',(e)=>{
        e.preventDefault();
        if($('form').parsley().validate()){
           notification('success','', 'Student added!');
           formReset('hide');
        }
    });

});