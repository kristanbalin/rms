      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
          <!-- partial:partials/_sidebar.html -->
          <nav class="sidebar sidebar-offcanvas" id="sidebar">
              <ul class="nav">
                  <li class="nav-item nav-profile">
                      <a href="#" class="nav-link">
                          <div class="nav-profile-image">
                              <img src="assets/images/faces/face1.jpg" alt="profile">
                              <span class="login-status online"></span>
                              <!--change to offline or busy as needed-->
                          </div>
                          <div class="nav-profile-text d-flex flex-column">
                              <span class="font-weight-bold mb-2">
                              <?php echo USERNAME; ?>
                              </span>
                              <!-- <span class="text-secondary text-small">Project Manager</span> -->
                          </div>
                          <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="index.php">
                          <span class="menu-title">Dashboard</span>
                          <i class="mdi mdi-home menu-icon"></i>
                      </a>
                  </li>

                  <li class="nav-item">
                      <a class="nav-link" href="student-info.php">
                          <span class="menu-title">Student Info</span>
                          <i class="mdi mdi-contacts menu-icon"></i>
                      </a>
                  </li>

                  <li class="nav-item">
                      <a class="nav-link" href="faculty-info.php">
                          <span class="menu-title">Faculty Info</span>
                          <i class="mdi mdi-contacts menu-icon"></i>
                      </a>
                  </li>

                  <li class="nav-item">
                      <a class="nav-link" href="curriculum-info.php">
                          <span class="menu-title">Curriculum Info</span>
                          <i class="mdi mdi-contacts menu-icon"></i>
                      </a>
                  </li>

                  <li class="nav-item">
                      <a class="nav-link" href="grade-sheet.php">
                          <span class="menu-title">Grade Sheet</span>
                          <i class="mdi mdi-contacts menu-icon"></i>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" data-toggle="collapse" href="#general-pages" aria-expanded="false" aria-controls="general-pages">
                          <span class="menu-title">Error Pages</span>
                          <i class="menu-arrow"></i>
                          <i class="mdi mdi-medical-bag menu-icon"></i>
                      </a>
                      <div class="collapse" id="general-pages">
                          <ul class="nav flex-column sub-menu">

                              <li class="nav-item"> <a class="nav-link" href="error-404.php"> 404 </a></li>
                              <li class="nav-item"> <a class="nav-link" href="error-500.php"> 500 </a></li>
                          </ul>
                      </div>
                  </li>
              </ul>


          </nav>