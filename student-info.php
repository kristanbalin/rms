<?php
include 'templates/header.php';
include 'templates/sidebar.php';
?>

<!-- partial -->
<div class="main-panel">
  <div class="content-wrapper">
    <div class="row">

      <!-- student form -->
      <div class="col-lg-12 grid-margin stretch-card" id = "div_form">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Add Student</h4>
            <form class="form-sample">
            <p class="card-description"> Student Information </p>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-6 col-form-label">Student Number</label>
                    <div class="col-sm-12">
                      <input type="text" class="form-control" 
                      placeholder = "20XX-00XXXX-CM-0" 
                      data-parsley-required="true"/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-6 col-form-label">Date of Admission</label>
                    <div class="col-sm-12">
                      <input class="form-control" placeholder="dd/mm/yyyy" 
                      data-parsley-required="true"/>
                    </div>
                  </div>
                </div>
              </div>
              <p class="card-description"> Profile </p>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group row">
                    <label class="col-sm-12 col-form-label">First Name</label>
                    <div class="col-sm-12">
                      <input type="text" class="form-control" 
                      data-parsley-required="true"/>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group row">
                    <label class="col-sm-12 col-form-label">Middle Name</label>
                    <div class="col-sm-12">
                      <input type="text" class="form-control" />
                    </div>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group row">
                    <label class="col-sm-12 col-form-label">Last Name</label>
                    <div class="col-sm-12">
                      <input type="text" class="form-control" 
                      data-parsley-required="true"/>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-5 col-form-label">Gender</label>
                    <div class="col-sm-12">
                      <select class="form-control">
                        <option>Male</option>
                        <option>Female</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-5 col-form-label">Date of Birth</label>
                    <div class="col-sm-12">
                      <input class="form-control" placeholder="dd/mm/yyyy"
                      data-parsley-required="true" />
                    </div>
                  </div>
                </div>
              </div>

              <p class="card-description"> Address </p>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-5 col-form-label">Address</label>
                    <div class="col-sm-12">
                      <input type="text" class="form-control"
                      data-parsley-required="true" />
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-sm-6 col-form-label">City or Province</label>
                    <div class="col-sm-12">
                      <input type="text" class="form-control"
                      data-parsley-required="true" />
                    </div>
                  </div>
                </div>
              </div>
  


              <div class="text-right">
                <button type="reset" class="btn btn-light" onclick = "formReset('hide');">Cancel</button>
                <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- student form:end -->

      <!-- student table -->
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Student Information</h4>
            <p class="card-description"> List of Students
            <button type="button" 
            class="btn btn-primary"  
            style="margin-left:80%" onclick="formReset('show')">
                 <i class="fas fa-plus"></i> Add </button>
            </p>

            <table class="table table-striped" id = 'student_info'>
              <thead>
                <tr>
 
                  <th> Student Number </th>
                  <th> Name </th>
                  <th> Address </th>
                  <th> Date of Admission </th>
                </tr>
              </thead>
              <tbody>
                <tr>

                  <td> 2018-00XXXX-CM-0 </td>

                  <td> Herman Human</td>

                  <td> Some place</td>
                  <td> May 15, 2022 </td>
                </tr>
                <tr>

                  <td> 2018-00XXXX-CM-0 </td>

                  <td> Messsy Human </td>

                  <td> Some place</td>
                  <td> July 1, 2022  </td>
                </tr>
                <tr>
 
                  <td> 2018-00XXXX-CM-0 </td>

                  <td> John Human </td>

                  <td> Some place </td>
                  <td> Apr 12, 2022  </td>
                </tr>
                <tr>

                  <td> 2018-00XXXX-CM-0 </td>

                  <td> Peter Human </td>

                  <td> Some place </td>
                  <td> May 15, 2022  </td>
                </tr>
                <tr>

                  <td> 2018-00XXXX-CM-0 </td>

                  <td> Edward Human </td>

                  <td> Some place </td>
                  <td> May 03, 2022  </td>
                </tr>
                <tr>

                  <td> 2018-00XXXX-CM-0 </td>

                  <td> John Human </td>

                  <td> Some place </td>
                  <td> April 05, 2022  </td>
                </tr>
                <tr>

                  <td> 2018-00XXXX-CM-0 </td>

                  <td> Henry Human </td>

                  <td> Some place </td>
                  <td> June 16, 2022 </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- student table:end -->



    </div>
  </div>
  <?php
  include 'templates/footer.php';
  ?>
  <script>
  //set title
document.title = "RMS - Student Information";
  </script>